<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Thank you for registration</title>
    <link rel="stylesheet" href="inc/css/bootstrap.min.css">
    <style>
        body{
            background: lightblue;
            padding: 100px;
        }
        .thankyou-content{
            width: 850px;
            margin: 0 auto;
        }
        img{
            margin-top: 15px;
        }

    </style>
</head>
<body>
    <div class="thankyou-content text-center">
        <h3 style="line-height: 1.5">Thank you for registering. <br> Now your account is under maintenance.</h3>
        <img src="inc/img/email.png" alt="" width="100" height="100">
		<h4 style="margin-top: 80px;">After approval of your account, we will send you a email. <br> Thank you...</h4>
		<p><a href="index.php" style="text-decoration: underline; font-weight: bold; font-size: 20px">Home</a></p>
    </div>

    <script src="inc/js/jquery-2.2.4.min.js"></script>
    <script src="inc/js/bootstrap.min.js"></script>
</body>
</html>






